/*
 * Copyright (c) 2003 Fabrice Bellard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @file
 * libavformat API example.
 *
 * Output a media file in any supported libavformat format. The default
 * codecs are used.
 * @example muxing.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#include <libavutil/avassert.h>
#include <libavutil/channel_layout.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>



#include "libavutil/avstring.h"
#include "libavutil/dict.h"
#include "libavutil/opt.h"
#include "libavutil/parseutils.h"
#include "libavutil/pixdesc.h"
#include "libavutil/time.h"
#include "libavcodec/bytestream.h"
#include "libavcodec/internal.h"
#include "libavcodec/options_table.h"



#define STREAM_DURATION   10.0
#define STREAM_FRAME_RATE 25 /* 25 images/s */
#define STREAM_PIX_FMT    AV_PIX_FMT_YUV420P /* default pix_fmt */
#define SCALE_FLAGS SWS_BICUBIC

#define RELATIVE_TS_BASE (INT64_MAX - (1LL<<48))
#define MAX_PROBE_PACKETS 2500

FILE *videofile;
FILE *audiofile;
FILE *videofile_header;
FILE *audiofile_header;
char audio_header_file_read;
char video_header_file_read;
long int audiosiz;
long int videosiz;
// a wrapper around a single output AVStream
typedef struct OutputStream {
	AVStream *st;
} OutputStream;

static AVCodec libx264_codec = {
	.name = "libx264",
	.long_name = "libx264",
	.type = AVMEDIA_TYPE_VIDEO,
	.id = AV_CODEC_ID_H264,
	.priv_data_size = 1232,
	.init = 0,
	.encode2 = 0,
	.close = 0,
	.capabilities = AV_CODEC_CAP_DELAY | AV_CODEC_CAP_AUTO_THREADS,
	.priv_class = 0,
	.defaults = 0,
	.init_static_data = 0,
	.caps_internal = FF_CODEC_CAP_INIT_THREADSAFE |
	FF_CODEC_CAP_INIT_CLEANUP,
};
static AVCodec aac_codec = {
	.name = "aac",
	.long_name = "AAC (Advanced Audio Coding)",
	.type = AVMEDIA_TYPE_AUDIO,
	.id = AV_CODEC_ID_AAC,
	.priv_data_size = 566880,
	.init = 0,
	.close = 0,
	.decode = 0,
	.sample_fmts = (const enum AVSampleFormat[]) {
		AV_SAMPLE_FMT_FLTP, AV_SAMPLE_FMT_NONE
	},
	.capabilities = AV_CODEC_CAP_CHANNEL_CONF | AV_CODEC_CAP_DR1,
	.caps_internal = FF_CODEC_CAP_INIT_THREADSAFE,
	.channel_layouts = 0,
	.flush = 0,
	.priv_class = 0,
	.profiles = 0,
};

static void my_log_callback(void *ptr, int level, const char *fmt, va_list vargs)
{
	vfprintf(stdout, fmt, vargs);
}

static int read_AVPacket_fromfile(FILE *file, AVPacket *pkt, int64_t pts)
{
	int64_t dts;
	int size;
	if(!feof(file)) {
		fread(&dts, sizeof(int64_t), 1, file);
		fread(&size, sizeof(int), 1, file);
		pkt->dts = pts;
		pkt->pts = pts;
		pkt->size = size;
		if(pkt->data)
			free(pkt->data);
		pkt->data = (char*)malloc(pkt->size * sizeof(char));
		fread(pkt->data, sizeof(char), pkt->size, file);
	} else
		return 0;
	return size;
}
static int read_audiofile_header(FILE *file, AVRational *time_base)
{
	fread(&time_base->num, sizeof(int), 1, file);
	fread(&time_base->den, sizeof(int), 1, file);
	return 0;
}

static int read_header_fromfile(FILE *file, AVStream* st)
{
	fread(&st->time_base.num, sizeof(st->time_base.num), 1, file);
	fread(&st->time_base.den, sizeof(st->time_base.den), 1, file);
	fread(&st->codec->width, sizeof(st->codec->width), 1, file);
	fread(&st->codec->height, sizeof(st->codec->height), 1, file);
	fread(&st->codec->extradata_size, sizeof(st->codec->extradata_size), 1, file);
	st->codec->extradata = av_mallocz(st->codec->extradata_size + FF_INPUT_BUFFER_PADDING_SIZE);
	fread(st->codec->extradata, 1, st->codec->extradata_size, file);
	return 0;
}

static int write_frame(AVFormatContext *fmt_ctx, const AVRational *time_base, AVStream *st, AVPacket *pkt)
{
	/* rescale output packet timestamp values from codec to stream timebase */
	av_packet_rescale_ts(pkt, *time_base, st->time_base);
	pkt->stream_index = st->index;

	return av_interleaved_write_frame(fmt_ctx, pkt);
}


static void add_stream(OutputStream *ost, AVFormatContext *oc, AVCodec *codec)
{
	AVCodecContext *c;
	int i;

	ost->st = avformat_new_stream(oc, codec);
	//ost->st = my_avformat_new_stream(oc, av);//*codec);
	if(!ost->st) {
		fprintf(stderr, "Could not allocate stream\n");
		exit(1);
	}
	
	ost->st->id = oc->nb_streams - 1;
	
	c = ost->st->codec;

	c->codec_type = codec->type;

	switch(codec->type) {
	case AVMEDIA_TYPE_AUDIO:
		c->codec_id = codec->id;
		i = read_audiofile_header(audiofile_header, &ost->st->time_base);
		c->sample_fmt = codec->sample_fmts ? codec->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;
		c->sample_rate = 44100;
		c->bit_rate = 64000;
		c->channel_layout = AV_CH_LAYOUT_STEREO;
		c->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
		ost->st->time_base = (AVRational){ 1, c->sample_rate };
		c->time_base = ost->st->time_base;
		ost->st->codec->frame_size = 1024;
		//c->strict_std_compliance = -2; //////////////////////////////////////////////FOR MP4
		break;

	case AVMEDIA_TYPE_VIDEO:
		c->codec_id = codec->id;
		c->bit_rate = 400000;
		c->pix_fmt = AV_PIX_FMT_YUV420P;
		i = read_header_fromfile(videofile_header, ost->st);
		c->time_base = ost->st->time_base;
		c->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
		break;

	default:
		break;
	}
}

/*
 * encode one audio frame and send it to the muxer
 * return 1 when encoding is finished, 0 otherwise
 */
static int read_audio_frame(AVFormatContext *oc, OutputStream *ost, int64_t pts)
{
	AVCodecContext *c;
	AVPacket pkt = {0};
	int ret;
	c = ost->st->codec;

	av_init_packet(&pkt);

	ret = read_AVPacket_fromfile(audiofile, &pkt, pts);
	if(ret > 0)
		audiosiz += ret;
	if(ret > 0)
		ret = write_frame(oc, &c->time_base, ost->st, &pkt);

	if(ret < 0) {
		fprintf(stderr, "Error while writing audio frame: %s\n", av_err2str(ret));
		exit(1);
	}

	return 0;
}

/*
 * encode one video frame and send it to the muxer
 * return 1 when encoding is finished, 0 otherwise
 */
static int read_video_frame(AVFormatContext *oc, OutputStream *ost, int64_t pts)
{
	int ret;
	AVCodecContext *c;
	AVPacket pkt = {0};
	c = ost->st->codec;

	av_init_packet(&pkt);

	ret = read_AVPacket_fromfile(videofile, &pkt, pts);
	if(ret > 0)
		videosiz += ret;
	if(ret > 0)
		ret = write_frame(oc, &c->time_base, ost->st, &pkt);

	if(ret < 0) {
		fprintf(stderr, "Error while writing video frame: %s\n", av_err2str(ret));
		exit(1);
	}

	return 0;
}
static void close_stream(AVFormatContext *oc, OutputStream *ost)
{
	avcodec_close(ost->st->codec);
}

/**************************************************************/
/* media file output */

int main_demux(int argc, char **argv)
{
	OutputStream video_st = {0}, audio_st = {0};
	const char *filename;
	AVOutputFormat *fmt;
	AVFormatContext *oc;
	AVCodec *audio_codec, *video_codec;
	int ret;
	int have_video = 0, have_audio = 0;
	int encode_video = 0, encode_audio = 0;
	AVDictionary *opt = NULL;
	audio_header_file_read = 0;
	video_header_file_read = 0;
	audiosiz = 0;
	videosiz = 0;
	videofile = fopen("videofile.bin", "rb");
	if(videofile == NULL) {
		printf("error creating file");
		return -1;
	}
	audiofile = fopen("audiofile.bin", "rb");
	if(audiofile == NULL) {
		printf("error creating file");
		return -1;
	}
	videofile_header = fopen("videofile_header.bin", "rb");
	if(videofile_header == NULL) {
		printf("error creating file");
		return -1;
	}
	audiofile_header = fopen("audiofile_header.bin", "rb");
	if(audiofile_header == NULL) {
		printf("error creating file");
		return -1;
	}
	av_register_all();
	avformat_network_init();
	av_log_set_callback(my_log_callback);
	filename = "h_4.flv";
	//const char *m_output="rtmp://52.18.44.240:80/skgl-07df09120e2f040805de/0ab3309454ed01d0842639a03264afec149787722bbbb46e92002bb65068d48e";
	avformat_alloc_output_context2(&oc, NULL, "flv", filename);
	//avformat_alloc_output_context2(&oc, NULL, "flv", m_output);
	if(!oc) {
		printf("Could not deduce output format from file extension: using MPEG.\n");
		avformat_alloc_output_context2(&oc, NULL, "flv", filename);
		//avformat_alloc_output_context2(&oc, NULL, "flv", m_output);
	}
	if(!oc)
		return 1;
	avformat_alloc_context();
	fmt = oc->oformat;

	add_stream(&video_st, oc, &libx264_codec);
	have_video = 1;
	encode_video = 1;

	add_stream(&audio_st, oc, &aac_codec);
	have_audio = 1;
	encode_audio = 1;


	av_dump_format(oc, 0, filename, 1);
	//av_dump_format(oc, 0, m_output, 1);

	if(!(fmt->flags & AVFMT_NOFILE)) {
		ret = avio_open(&oc->pb, filename, AVIO_FLAG_WRITE);
		//ret = avio_open(&oc->pb, m_output, AVIO_FLAG_WRITE);
		if(ret < 0) {
			fprintf(stderr, "Could not open '%s': %s\n", filename, av_err2str(ret));
			//fprintf(stderr, "Could not open '%s': %s\n", m_output, av_err2str(ret));
			return 1;
		}
	}

	ret = avformat_write_header(oc, &opt);
	if(ret < 0) {
		fprintf(stderr, "Error occurred when opening output file: %s\n", av_err2str(ret));
		return 1;
	}

	int64_t pts_audio, pts_video;
	fread(&pts_video, sizeof(int64_t), 1, videofile);
	fread(&pts_audio, sizeof(int64_t), 1, audiofile);
	int64_t pts_videos=0;
	int64_t pts_audios=0;
	while(encode_video || encode_audio) {
		if(encode_video && (!encode_audio || av_compare_ts(pts_videos, video_st.st->codec->time_base, pts_audios, audio_st.st->codec->time_base) <= 0)) {
			encode_video = !read_video_frame(oc, &video_st, pts_videos);
			if(!feof(videofile)) {
				fread(&pts_video, sizeof(int64_t), 1, videofile);
				pts_videos+=40;
			}
			else
				encode_video = 0;
		} else {
			encode_audio = !read_audio_frame(oc, &audio_st, pts_audio);
			if(!feof(audiofile)) {
				fread(&pts_audio, sizeof(int64_t), 1, audiofile);
			}
			else
				encode_audio = 0;
		}
	}

	av_write_trailer(oc);

	if(have_video)
		close_stream(oc, &video_st);

	if(have_audio)
		close_stream(oc, &audio_st);

	if(!(fmt->flags & AVFMT_NOFILE))
		avio_closep(&oc->pb);

	avformat_free_context(oc);

	fclose(audiofile);
	fclose(videofile);
	fclose(audiofile_header);
	fclose(videofile_header);

	return 0;
}

int main_mux(int argc, char **argv);

int main(int argc, char **argv)
{
	main_mux(argc, argv);
	return main_demux(argc, argv);
}
